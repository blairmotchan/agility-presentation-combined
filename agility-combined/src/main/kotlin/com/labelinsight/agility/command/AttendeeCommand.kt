package com.labelinsight.agility.command

data class AttendeeCommand(val name: String)