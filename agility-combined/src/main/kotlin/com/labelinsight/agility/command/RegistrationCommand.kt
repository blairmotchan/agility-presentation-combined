package com.labelinsight.agility.command

data class RegistrationCommand(val attendeeId: Long, val sessionId: Long)