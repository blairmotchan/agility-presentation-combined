package com.labelinsight.agility.command

data class SessionCommand(val name: String)